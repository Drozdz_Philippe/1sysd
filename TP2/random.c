#include<stdio.h>
#include<stdlib.h>  // cf man 3 rand
#include<time.h> 

// expérimentations avec rand


int main() {
    int hasard;
	
    srand(time(NULL)); // initialise la graine/seed avec l'epoch

    printf("RAND_MAX = %d\n", RAND_MAX); // défini dans stdlib.h

    hasard = rand();
    printf("Tirage : %d\n", hasard);
    hasard = rand();
    printf("Tirage : %d\n", hasard);
    hasard = rand();
    printf("Tirage : %d\n", hasard);
    hasard = rand();
    printf("Tirage : %d\n", hasard);
    hasard = rand();
    printf("Tirage : %d\n", hasard);
    hasard = rand();
    printf("Tirage : %d\n", hasard);
}

