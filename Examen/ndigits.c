#include <stdio.h>
#include <ctype.h>  

int n_digits(char *s) {
    int count = 0;  
    while (*s) {   
        if (isdigit(*s)) {  
            count++; 
        }
        s++;  
    }
    return count;  
}

int main(int argc, char *argv[]) {
  
      	if (argc != 2) {
        printf("Erreur : vous devez fournir au minimum un argument.\n");
        return 1;  
    }

   	 int count = n_digits(argv[1]);
   	 printf("Le nombre de chiffres dans l'argument est : %d\n", count);
	 return 0;  
}

