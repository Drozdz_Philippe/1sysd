#include <stdio.h>

void display_histogram(int tab[], int size) {
    int max = 0;
    for (int i = 0; i < size; i++) {
        if (tab[i] > max) {
            max = tab[i];
        }
    }

    for (int level = max; level >= 1; level--) {
        printf("%2d| ", level); 
        for (int i = 0; i < size; i++) {
            if (tab[i] >= level) {
                printf("*  ");
            } else {
                printf("   ");
            }
        }
        printf("\n");
    }

    printf(" 0| ");
    for (int i = 0; i < size; i++) {
        printf("%-2d ", tab[i]);
    }
    printf("\n");
}

int main() {
    int values[] = { 4, 9, 8, 2, 0, 1, 10, 5, 8 };
    int size = sizeof(values) / sizeof(values[0]);

    display_histogram(values, size);

    return 0;
}

